overbooking <- 15
asientos <- 180
n <- overbooking + asientos
p <- 0.9 #probabilidad abordar el avión
x <- seq(1,n)
y <- dbinom(x,n,p)
plot(x,y,col="blue", xlab="pasajeros abordando", ylab="probabilidad")

#Aproximación normal
m <- n*p #aproximación promedio
s <- sqrt(n*p*(1-p)) #aproximación desviación estandar

yn <- dnorm(x, mean = m, sd = s)
lines(x,yn,type="l",col="red")


revenue <- function(x){
  penalidad <-800
  precio <-250
  p <- 0.9
  dist <- dbinom(seq(181,x),x,p)
  return (x*precio - sum(dist*seq(1:(x-180))*penalidad))
}
data <- data.frame("tickets" = seq(181,210))
data$revenue <- apply(data,1,revenue)
maxRevenue = max(data$revenue)
ticketMax = data$tickets[which.max(data$revenue)]
cat("Mayor Ganancia:",maxRevenue)
cat("Mayor cantidad de Tickets a Vender:",ticketMax)
plot(data$tickets,data$revenue,xlab="tickets vendidos", ylab="revenue")
